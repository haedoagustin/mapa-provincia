var regions =
    [
        {
            "region_name": "PATAGONES",
            "region_code": 92,
            "Habitantes": 30207
        },
        {
            "region_name": "VILLARINO",
            "region_code": 195,
            "Habitantes": 31014
        },
        {
            "region_name": "CORONEL ROSALES",
            "region_code": 99,
            "Habitantes": 62152
        },
        {
            "region_name": "BAHIA BLANCA",
            "region_code": 75,
            "Habitantes": 301572
        },
        {
            "region_name": "TORNQUIST",
            "region_code": 189,
            "Habitantes": 12723
        },
        {
            "region_name": "MONTE HERMOSO",
            "region_code": 147,
            "Habitantes": 6499
        },
        {
            "region_name": "CORONEL DORREGO",
            "region_code": 100,
            "Habitantes": 15825
        },
        {
            "region_name": "CORONEL PRINGLES",
            "region_code": 101,
            "Habitantes": 22933
        },
        {
            "region_name": "TRES ARROYOS",
            "region_code": 191,
            "Habitantes": 57110
        },
        {
            "region_name": "ADOLFO GONZALES CHAVES",
            "region_code": 66,
            "Habitantes": 12047
        },
        {
            "region_name": "SAN CAYETANO",
            "region_code": 175,
            "Habitantes": 8399
        },
        {
            "region_name": "GENERAL ALVARADO",
            "region_code": 69,
            "Habitantes": 39594
        },
        {
            "region_name": "LOBERIA",
            "region_code": 136,
            "Habitantes": 17523
        },
        {
            "region_name": "NECOCHEA",
            "region_code": 151,
            "Habitantes": 92933
        },
        {
            "region_name": "BENITO JUAREZ",
            "region_code": 80,
            "Habitantes": 20239
        },
        {
            "region_name": "LAPRIDA",
            "region_code": 131,
            "Habitantes": 10210
        },
        {
            "region_name": "GENERAL LA MADRID",
            "region_code": 129,
            "Habitantes": 10783
        },
        {
            "region_name": "CORONEL SUAREZ",
            "region_code": 102,
            "Habitantes": 38320
        },
        {
            "region_name": "SAAVEDRA",
            "region_code": 169,
            "Habitantes": 20749
        },
        {
            "region_name": "PUAN",
            "region_code": 161,
            "Habitantes": 15743
        },
        {
            "region_name": "OLAVARRIA",
            "region_code": 154,
            "Habitantes": 111708
        },
        {
            "region_name": "DAIREAUX",
            "region_code": 103,
            "Habitantes": 16889
        },
        {
            "region_name": "GUAMINI",
            "region_code": 121,
            "Habitantes": 11826
        },
        {
            "region_name": "TRES LOMAS",
            "region_code": 922,
            "Habitantes": 8700
        },
        {
            "region_name": "SALLIQUELO",
            "region_code": 171,
            "Habitantes": 8644
        },
        {
            "region_name": "ADOLFO ALSINA",
            "region_code": 65,
            "Habitantes": 17072
        },
        {
            "region_name": "BOLIVAR",
            "region_code": 83,
            "Habitantes": 34190
        },
        {
            "region_name": "HIPOLITO YRIGOYEN",
            "region_code": 196,
            "Habitantes": 9585
        },
        {
            "region_name": "CARLOS CASARES",
            "region_code": 89,
            "Habitantes": 22237
        },
        {
            "region_name": "PEHUAJO",
            "region_code": 155,
            "Habitantes": 39776
        },
        {
            "region_name": "TRENQUE LAUQUEN",
            "region_code": 190,
            "Habitantes": 43021
        },
        {
            "region_name": "PELLEGRINI",
            "region_code": 156,
            "Habitantes": 5887
        },
        {
            "region_name": "CARLOS TEJEDOR",
            "region_code": 90,
            "Habitantes": 11570
        },
        {
            "region_name": "RIVADAVIA",
            "region_code": 166,
            "Habitantes": 17143
        },
        {
            "region_name": "GENERAL VILLEGAS",
            "region_code": 120,
            "Habitantes": 30864
        },
        {
            "region_name": "FLORENTINO AMEGHINO",
            "region_code": 71,
            "Habitantes": 8869
        },
        {
            "region_name": "GENERAL PINTO",
            "region_code": 117,
            "Habitantes": 11261
        },
        {
            "region_name": "GENERAL ALVEAR",
            "region_code": 70,
            "Habitantes": 11130
        },
        {
            "region_name": "TAPALQUE",
            "region_code": 186,
            "Habitantes": 9178
        },
        {
            "region_name": "AZUL",
            "region_code": 74,
            "Habitantes": 65280
        },
        {
            "region_name": "TANDIL",
            "region_code": 185,
            "Habitantes": 123871
        },
        {
            "region_name": "BALCARCE",
            "region_code": 76,
            "Habitantes": 43823
        },
        {
            "region_name": "GENERAL PUEYRREDON",
            "region_code": 162,
            "Habitantes": 618989
        },
        {
            "region_name": "MAR CHIQUITA",
            "region_code": 143,
            "Habitantes": 21279
        },
        {
            "region_name": "AYACUCHO",
            "region_code": 73,
            "Habitantes": 20337
        },
        {
            "region_name": "RAUCH",
            "region_code": 165,
            "Habitantes": 15176
        },
        {
            "region_name": "LAS FLORES",
            "region_code": 132,
            "Habitantes": 23871
        },
        {
            "region_name": "SALADILLO",
            "region_code": 170,
            "Habitantes": 32103
        },
        {
            "region_name": "ROQUE PEREZ",
            "region_code": 168,
            "Habitantes": 12513
        },
        {
            "region_name": "VEINTICINCO DE MAYO",
            "region_code": 63,
            "Habitantes": 35842
        },
        {
            "region_name": "NUEVE DE JULIO",
            "region_code": 64,
            "Habitantes": 47722
        },
        {
            "region_name": "LINCOLN",
            "region_code": 135,
            "Habitantes": 41808
        },
        {
            "region_name": "LEANDRO N. ALEM",
            "region_code": 134,
            "Habitantes": 16799
        },
        {
            "region_name": "ALBERTI",
            "region_code": 67,
            "Habitantes": 10654
        },
        {
            "region_name": "BRAGADO",
            "region_code": 84,
            "Habitantes": 41336
        },
        {
            "region_name": "GENERAL VIAMONTE",
            "region_code": 193,
            "Habitantes": 18078
        },
        {
            "region_name": "JUNIN",
            "region_code": 126,
            "Habitantes": 90305
        },
        {
            "region_name": "GENERAL ARENALES",
            "region_code": 111,
            "Habitantes": 14903
        },
        {
            "region_name": "CHACABUCO",
            "region_code": 95,
            "Habitantes": 48703
        },
        {
            "region_name": "ROJAS",
            "region_code": 167,
            "Habitantes": 23432
        },
        {
            "region_name": "COLON",
            "region_code": 98,
            "Habitantes": 24890
        },
        {
            "region_name": "PERGAMINO",
            "region_code": 157,
            "Habitantes": 104590
        },
        {
            "region_name": "SALTO",
            "region_code": 172,
            "Habitantes": 32653
        },
        {
            "region_name": "ARRECIFES",
            "region_code": 78,
            "Habitantes": 29044
        },
        {
            "region_name": "CAPITAN SARMIENTO",
            "region_code": 88,
            "Habitantes": 14494
        },
        {
            "region_name": "CARMEN DE ARECO",
            "region_code": 91,
            "Habitantes": 14692
        },
        {
            "region_name": "SAN ANTONIO DE ARECO",
            "region_code": 174,
            "Habitantes": 23138
        },
        {
            "region_name": "SAN NICOLAS",
            "region_code": 951,
            "Habitantes": 145857
        },
        {
            "region_name": "RAMALLO",
            "region_code": 164,
            "Habitantes": 33042
        },
        {
            "region_name": "SAN PEDRO",
            "region_code": 182,
            "Habitantes": 59036
        },
        {
            "region_name": "BARADERO",
            "region_code": 77,
            "Habitantes": 32761
        },
        {
            "region_name": "ZARATE",
            "region_code": 197,
            "Habitantes": 114269
        },
        {
            "region_name": "CAMPANA",
            "region_code": 86,
            "Habitantes": 94461
        },
        {
            "region_name": "EXALTACION DE LA CRUZ",
            "region_code": 108,
            "Habitantes": 29805
        },
        {
            "region_name": "SAN ANDRES DE GILES",
            "region_code": 173,
            "Habitantes": 23027
        },
        {
            "region_name": "LUJAN",
            "region_code": 139,
            "Habitantes": 106273
        },
        {
            "region_name": "MERCEDES",
            "region_code": 145,
            "Habitantes": 63284
        },
        {
            "region_name": "SUIPACHA",
            "region_code": 184,
            "Habitantes": 10081
        },
        {
            "region_name": "CHIVILCOY",
            "region_code": 97,
            "Habitantes": 64185
        },
        {
            "region_name": "GENERAL LAS HERAS",
            "region_code": 133,
            "Habitantes": 14889
        },
        {
            "region_name": "NAVARRO",
            "region_code": 150,
            "Habitantes": 17054
        },
        {
            "region_name": "LOBOS",
            "region_code": 137,
            "Habitantes": 36172
        },
        {
            "region_name": "PILA",
            "region_code": 158,
            "Habitantes": 3640
        },
        {
            "region_name": "GENERAL GUIDO",
            "region_code": 113,
            "Habitantes": 2816
        },
        {
            "region_name": "MAIPU",
            "region_code": 141,
            "Habitantes": 10188
        },
        {
            "region_name": "GENERAL MADARIAGA",
            "region_code": 115,
            "Habitantes": 19747
        },
        {
            "region_name": "VILLA GESELL",
            "region_code": 412,
            "Habitantes": 31730
        },
        {
            "region_name": "PINAMAR",
            "region_code": 410,
            "Habitantes": 25728
        },
        {
            "region_name": "LA COSTA",
            "region_code": 411,
            "Habitantes": 69633
        },
        {
            "region_name": "GENERAL LAVALLE",
            "region_code": 114,
            "Habitantes": 3700
        },
        {
            "region_name": "DOLORES",
            "region_code": 104,
            "Habitantes": 27042
        },
        {
            "region_name": "TORDILLO",
            "region_code": 188,
            "Habitantes": 1764
        },
        {
            "region_name": "CASTELLI",
            "region_code": 94,
            "Habitantes": 8205
        },
        {
            "region_name": "CA&Ntilde;UELAS",
            "region_code": 957,
            "Habitantes": 51892
        },
        {
            "region_name": "SAN VICENTE",
            "region_code": 183,
            "Habitantes": 59478
        },
        {
            "region_name": "PRESIDENTE PERON",
            "region_code": 953,
            "Habitantes": 81141
        },
        {
            "region_name": "ESTEBAN ECHEVERRIA",
            "region_code": 407,
            "Habitantes": 300959
        },
        {
            "region_name": "EZEIZA",
            "region_code": 929,
            "Habitantes": 163722
        },
        {
            "region_name": "LEZAMA",
            "region_code": 956,
            "Habitantes": 8647
        },
        {
            "region_name": "MARCOS PAZ",
            "region_code": 144,
            "Habitantes": 54181
        },
        {
            "region_name": "GENERAL RODRIGUEZ",
            "region_code": 954,
            "Habitantes": 87185
        },
        {
            "region_name": "MORENO",
            "region_code": 939,
            "Habitantes": 452505
        },
        {
            "region_name": "JOSE C. PAZ",
            "region_code": 934,
            "Habitantes": 265981
        },
        {
            "region_name": "SAN MIGUEL",
            "region_code": 938,
            "Habitantes": 276190
        },
        {
            "region_name": "MALVINAS ARGENTINAS",
            "region_code": 930,
            "Habitantes": 322375
        },
        {
            "region_name": "PILAR",
            "region_code": 159,
            "Habitantes": 299077
        },
        {
            "region_name": "ESCOBAR",
            "region_code": 106,
            "Habitantes": 213619
        },
        {
            "region_name": "TIGRE",
            "region_code": 940,
            "Habitantes": 376381
        },
        {
            "region_name": "SAN FERNANDO (ISLAS)",
            "region_code": 958,
            "Habitantes": 163240
        },
        {
            "region_name": "SAN FERNANDO",
            "region_code": 925,
            "Habitantes": 163240
        },
        {
            "region_name": "SAN ISIDRO",
            "region_code": 177,
            "Habitantes": 292878
        },
        {
            "region_name": "VICENTE LOPEZ",
            "region_code": 952,
            "Habitantes": 269420
        },
        {
            "region_name": "HURLINGHAM",
            "region_code": 923,
            "Habitantes": 181241
        },
        {
            "region_name": "TRES DE FEBRERO",
            "region_code": 932,
            "Habitantes": 340071
        },
        {
            "region_name": "GENERAL SAN MARTIN",
            "region_code": 947,
            "Habitantes": 414196
        },
        {
            "region_name": "MORON",
            "region_code": 924,
            "Habitantes": 321109
        },
        {
            "region_name": "LA MATANZA",
            "region_code": 933,
            "Habitantes": 1775816
        },
        {
            "region_name": "MERLO",
            "region_code": 948,
            "Habitantes": 528494
        },
        {
            "region_name": "ITUZAINGO",
            "region_code": 926,
            "Habitantes": 167824
        },
        {
            "region_name": "FLORENCIO VARELA",
            "region_code": 949,
            "Habitantes": 426005
        },
        {
            "region_name": "AVELLANEDA",
            "region_code": 72,
            "Habitantes": 342677
        },
        {
            "region_name": "LOMAS DE ZAMORA",
            "region_code": 927,
            "Habitantes": 616279
        },
        {
            "region_name": "QUILMES",
            "region_code": 943,
            "Habitantes": 582943
        },
        {
            "region_name": "LANUS",
            "region_code": 130,
            "Habitantes": 459263
        },
        {
            "region_name": "BERAZATEGUI",
            "region_code": 950,
            "Habitantes": 324244
        },
        {
            "region_name": "ALMIRANTE BROWN",
            "region_code": 955,
            "Habitantes": 552902
        },
        {
            "region_name": "MAGDALENA",
            "region_code": 140,
            "Habitantes": 19301
        },
        {
            "region_name": "PUNTA INDIO",
            "region_code": 409,
            "Habitantes": 9888
        },
        {
            "region_name": "GENERAL BELGRANO",
            "region_code": 112,
            "Habitantes": 17365
        },
        {
            "region_name": "LA PLATA",
            "region_code": 128,
            "Habitantes": 654324
        },
        {
            "region_name": "CHASCOMUS",
            "region_code": 96,
            "Habitantes": 29607
        },
        {
            "region_name": "GENERAL PAZ",
            "region_code": 116,
            "Habitantes": 11202
        },
        {
            "region_name": "ENSENADA",
            "region_code": 105,
            "Habitantes": 56729
        },
        {
            "region_name": "CORONEL BRANDSEN",
            "region_code": 85,
            "Habitantes": 26367
        },
        {
            "region_name": "BERISSO",
            "region_code": 82,
            "Habitantes": 88470
        },
        {
            "region_name": "MONTE",
            "region_code": 180,
            "Habitantes": 21034
        }
    ]

export default regions;